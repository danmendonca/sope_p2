############################################# MAKEFILE #############################################
## Compiler is gcc
CC=gcc
## Using -Wall as resquested
CFLAGS=-Wall -lrt -lpthread -lm -o

all: bin primes move

bin:
	mkdir bin


primes:
	$(CC) src/primes.c  src/circularQueue.c $(CFLAGS) primes

move:
	mv primes bin
