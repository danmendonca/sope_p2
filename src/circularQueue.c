#include "circularQueue.h"



void queue_init(CircularQueue **q, unsigned int capacity) {
	*q = (CircularQueue *) malloc(sizeof(CircularQueue));
	
	if (sem_init(&((*q)->empty), 0, capacity) != 0)
	{
		perror("ERROR creating semaphore\n");
		exit(SEM_ERR_CRT);
	}

	if (sem_init(&((*q)->full), 0, 0) != 0)
	{
		perror("ERROR creating semaphore\n");
		exit(SEM_ERR_CRT);
	}
 
	if (pthread_mutex_init(&((*q)->mutex), NULL) != 0)
	{
		perror("ERROR creating mutex\n");
		exit(MUTEX_ERR_CRT);
	}
	


	(*q)->v = (QueueElem *) malloc(capacity * sizeof(QueueElem)); 
	(*q)->capacity = capacity; 
	(*q)->first = 0;
	(*q)->last = 0;
}


// Inserts 'value' at the tail of queue 'q'
void queue_put(CircularQueue *q, QueueElem value) 
{ 
	if (sem_wait(&q->empty) != 0)
	{
		perror("ERROR waiting for semaphore\n");
		exit(SEM_ERR_WAIT);
	}

	if (pthread_mutex_lock(&q->mutex) != 0)
	{
		perror("ERROR locking mutex\n");
		exit(MUTEX_ERR_LOCK);
	}

	unsigned int index = q->last % q->capacity;

	q->v[index] = value;
	q->last++;
	

	if (pthread_mutex_unlock(&(q->mutex)) != 0)
	{
		perror("ERROR unlocking mutex\n");
		exit(MUTEX_ERR_UNLOCK);
	}

	if(sem_post(&q->full) != 0)
	{
		perror("ERROR signaling semaphore\n");
		exit(SEM_ERR_POST);
	}
} 


// Removes element at the head of queue 'q' and returns its 'value'
QueueElem queue_get(CircularQueue *q)
{
	if (sem_wait(&q->full) != 0)
	{
		perror("ERROR waiting for semaphore\n");
		exit(SEM_ERR_WAIT);
	}

	if (pthread_mutex_lock(&q->mutex) != 0)
	{
		perror("ERROR locking mutex\n");
		exit(MUTEX_ERR_LOCK);
	}

	unsigned int index = q->first % q->capacity;
	QueueElem first = q->v[index];
	q->first++;

	if (pthread_mutex_unlock(&(q->mutex)) != 0)
	{
		perror("ERROR unlocking mutex\n");
		exit(MUTEX_ERR_UNLOCK);
	}


	if (sem_post(&q->empty) != 0)
	{
		perror("ERROR signaling semaphore\n");
		exit(SEM_ERR_POST);
	}
	return first;
}


// Frees space allocated for the queue elements
void queue_destroy(CircularQueue *q)
{

	if (pthread_mutex_destroy(&q->mutex) != 0)
		{
			perror("ERROR destroying mutex\n");
			exit(MUTEX_ERR_DST);
		}

	if (sem_destroy(&q->empty) != 0)
	{
		perror("ERROR destroying semaphore\n");
		exit(SEM_ERR_DESTROY);
	}

	if (sem_destroy(&q->full) != 0)
	{
		perror("ERROR destroying semaphore\n");
		exit(SEM_ERR_DESTROY);
	}

	free(q->v);
	free(q);

	return;
}
