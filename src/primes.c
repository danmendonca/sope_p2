#include "primes.h"

struct primeList primes;
pthread_cond_t cond  = PTHREAD_COND_INITIALIZER;






//=====================================================================================================================
/**
 * this is the first thread that will be created
 * it will give the following thread, all numbers from 3 to n(user input)
 * if n == 2, then put it on the prime list and return
 */
void *thrFunc(void *arg)
{

	CircularQueue *departure_q;
	queue_init(&departure_q, QUEUE_SIZE);

	//if we only want primes untill 2
	if(primes.n==2){

		primes.primes[0] = 2;
		primes.index++;

		if (sem_post(&primes.complete) != 0)	//tell main thread that all primes are now into prime list
		{
			perror("ERROR signaling semaphore\n");
			exit(SEM_ERR_POST);
		}

		return NULL;
	}


	primes.thr_nr++; //increase count of threads running

	if (pthread_mutex_init(&(primes.mutex), NULL) != 0)	//initiate mutex, make sure that prime list isn't available to more that one thread at a time
	{
		perror("ERROR creating mutex\n");
		exit(MUTEX_ERR_CRT);
	}

	if (pthread_mutex_init(&(primes.thr_cnt_mutex), NULL) != 0)	//initiate mutex, make sure that this can't be changed by more than 1thread at a time
	{
		perror("ERROR creating mutex\n");
		exit(MUTEX_ERR_CRT);
	}

	pthread_t filter_thr;
	if (pthread_create(&filter_thr, NULL, filterThr, departure_q) != 0) 
	{
		perror("ERROR creating thread\n");
		exit(1);
	}

	if (pthread_detach(filter_thr) != 0)	//will not need to join
	{
		perror("ERROR detaching thread \n");
		exit(THREAD_ERR_DETACH);
	}


	QueueElem i;
	for(i=2; i<=primes.n; i++){
		queue_put(departure_q, i); //giving all numbers between 2 and the max possible prime given by the user
	}

	queue_put(departure_q, QUEUE_END); //sends the terminator symbol for the next thread



	//decrease count of threads running
	if (pthread_mutex_lock(&primes.thr_cnt_mutex) != 0)
	{
		perror("ERROR locking mutex\n");
		exit(MUTEX_ERR_LOCK);
	}

	primes.thr_nr--;

	if (pthread_mutex_unlock(&primes.thr_cnt_mutex) != 0)
	{
		perror("ERROR unlocking mutex\n");
		exit(MUTEX_ERR_UNLOCK);
	}


	return NULL;
}





//==========================================================================================================================
/**
 * this thread will filter all the multiple numbers of the first received, and give the others for the next thread
 */
void *filterThr(void *arg){



	CircularQueue *arrival_q = (CircularQueue *)arg;
	QueueElem control = queue_get(arrival_q); //gets the prime that will be used to filter the rest that will be received here

	if(control <2){
		perror("Unexpected control number received\n");
		exit(-1);
	}

	if(DEBUG)
		printf("filterThr control is %lu\n", control);


	/**
	 * If control * control > user_input, then, this is the last thread
	 */
	if((control * control) > primes.n)
	{
		do{		//every number read from the incoming circular queue is known to be a prime
			if (pthread_mutex_lock(&primes.mutex) != 0)
				{
					perror("ERROR locking mutex\n");
					exit(MUTEX_ERR_LOCK);
				}

				primes.primes[primes.index]= control;
				primes.index++;

				if (pthread_mutex_unlock(&(primes.mutex)) != 0)
				{
					perror("ERROR unlocking mutex\n");
					exit(MUTEX_ERR_UNLOCK);
				}
		}
		while((control=queue_get(arrival_q)) != QUEUE_END);


		queue_destroy(arrival_q);	//free the memory from the arriving queue



		if (pthread_mutex_lock(&(primes.thr_cnt_mutex)) != 0)
		{
			perror("ERROR unlocking mutex\n");
			exit(MUTEX_ERR_UNLOCK);
		}
		while(primes.thr_nr != 0)	//wait untill all the threads have written their control number into primes list
			if (pthread_cond_wait(&cond, &primes.thr_cnt_mutex) != 0)	//waiting for a thread to sign that has terminated, avoid busy waiting
			{
				perror("ERROR on conditional wait\n");
				exit(THREAD_ERR_CONDWAIT);
			}


		if (pthread_mutex_unlock(&(primes.thr_cnt_mutex)) != 0)
		{
			perror("ERROR unlocking mutex\n");
			exit(MUTEX_ERR_UNLOCK);
		}

		if (pthread_mutex_destroy(&primes.mutex) != 0)
		{
			perror("ERROR destroying mutex\n");
			exit(MUTEX_ERR_DST);
		}

		if (pthread_mutex_destroy(&primes.thr_cnt_mutex) != 0)
		{
			perror("ERROR destroying mutex\n");
			exit(MUTEX_ERR_DST);
		}

		if(pthread_cond_destroy(&cond)!=0){
			perror("ERROR destroying pthread_cond_t\n");
			exit(THREAD_ERR_DSTR_CW);
		}

		if (sem_post(&primes.complete) != 0)	//tell main thread that all primes are now into prime list
		{
			perror("ERROR signaling semaphore\n");
			exit(SEM_ERR_POST);
		}

		return NULL;
	}



	/**
	 * there may be numbers to filter yet
	 */

	//increase the count of threads running
	if (pthread_mutex_lock(&primes.thr_cnt_mutex) != 0)
	{
		perror("ERROR locking mutex\n");
		exit(MUTEX_ERR_LOCK);
	}

	primes.thr_nr++;

	if (pthread_mutex_unlock(&primes.thr_cnt_mutex) != 0)
	{
		perror("ERROR unlocking mutex\n");
		exit(MUTEX_ERR_LOCK);
	}


	CircularQueue *departure_q; 			//creates and initializes a circular queue for the next filter
	queue_init(&departure_q, QUEUE_SIZE);

	pthread_t filter_thr;
	QueueElem to_cmp;


	if (pthread_create(&filter_thr, NULL, filterThr, departure_q) != 0)	//creates the next filter thread
	{
		perror("ERROR creating thread\n");
		exit(1);
	}

	if (pthread_detach(filter_thr) != 0)	 //will not need to join this thread
	{
		perror("ERROR detaching thread \n");
		exit(THREAD_ERR_DETACH);
	}


	while((to_cmp=queue_get(arrival_q)) != QUEUE_END) //
	{
		if((to_cmp % control) != 0)
			queue_put(departure_q, to_cmp);	//add possible prime to the queue, for the next filter thread
	}


	queue_put(departure_q, QUEUE_END); //sends terminator for the next thread
	queue_destroy(arrival_q); //frees arrival_q, as it won't be needed anymore



	if (pthread_mutex_lock(&primes.mutex) != 0)
	{
		perror("ERROR locking mutex\n");
		exit(MUTEX_ERR_LOCK);
	}


	primes.primes[primes.index] = control; //adds control to the primes list
	primes.index++;


	if (pthread_mutex_unlock(&primes.mutex) != 0)
	{
		perror("ERROR locking mutex\n");
		exit(MUTEX_ERR_UNLOCK);
	}


	//decrease the number of running threads, we won't be accessing shared vars anymore
	if (pthread_mutex_lock(&primes.thr_cnt_mutex) != 0)
	{
		perror("ERROR locking mutex\n");
		exit(MUTEX_ERR_LOCK);
	}

	primes.thr_nr--;

	if (pthread_mutex_unlock(&primes.thr_cnt_mutex) != 0)
	{
		perror("ERROR locking mutex\n");
		exit(MUTEX_ERR_LOCK);
	}


	//sends a signal to the last thread warning that this one has terminated its work
	if(pthread_cond_signal(&cond)!=0)
	{
		perror("Error: Sending cond_signal");
		exit(THREAD_ERR_CONDWAIT);
	}

	return NULL;
}





//=========================================================================================================================
int main(int argc, char** argv) 
{


	//user input
	int long maxVal;

	if (argc != 2 || sscanf(argv[1], "%ld",  &maxVal) < 1 || maxVal <= 1)
	{
		printf("Usage: ./primes N, [N = max value], where N is a number > 1\n");
		exit(1);
	}



	primes.primes =  malloc(ceil(1.2 * ((maxVal) / log(maxVal)) * sizeof(double))); //allocate enough mem for saving the primes
	primes.index = 0; //let threads know the first available index to save the next prime
	primes.n= (QueueElem) maxVal; //user input, max prime to find
	primes.thr_nr = 0;	//number of threads running


	if (sem_init(&(primes.complete), 0, 0) != 0)	//initialize semaphore that will let us know when the primes list is done
	{
		perror("ERROR creating semaphore\n");
		exit(SEM_ERR_CRT);
	}

	pthread_t first_thr;
	if (pthread_create(&first_thr, NULL, thrFunc, NULL) != 0)
	{
		perror("ERROR creating thread\n");
		exit(1);
	}

	if (pthread_detach(first_thr) != 0)
	{
		perror("ERROR detaching thread \n");
		exit(THREAD_ERR_DETACH);
	}


	if (sem_wait(&primes.complete) != 0)
	{
		perror("ERROR waiting for semaphore\n");
		exit(SEM_ERR_WAIT);
	}

	//order and print the primes list
	sortList(primes.primes, primes.index+1);
	printList(primes.primes, primes.index+1);

	//destroy the semaphore
	if (sem_destroy(&primes.complete) != 0)
	{
		perror("ERROR destroying semaphore\n");
		exit(SEM_ERR_DESTROY);
	}

	return 0;
}





//=====================================================================================================================
//quicksort implementation
void sortList(QueueElem *primes, unsigned int size){
	unsigned int i;

	for(i=0; i<size-2; i++)
	{
		unsigned int j= i+1;
		for(; j<size-1; j++)
		{
			if(primes[i]>primes[j])
			{
				QueueElem tmp = primes[i];
				primes[i]= primes[j];
				primes[j] = tmp;
			}
		}
	}
}





//=====================================================================================================================
void printList(QueueElem *primes, unsigned int size){
	unsigned int i;

	for(i=0; i<size-1; i++){
		printf("#%u: ", i+1);
		printf("%lu \n", primes[i]);

		if(DEBUG)
			if(i>0 && i<(size-2) && primes[i]>primes[i+1])
				printf("Alert! not ordered\n");
	}
}
