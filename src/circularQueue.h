#ifndef circularQueue_H_

#define circularQueue_H_

#include <stdio.h>
#include <semaphore.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>


typedef unsigned long QueueElem; 
#define QUEUE_SIZE 10
#define DEBUG 0
#define MUTEX_ERR_CRT 1
#define MUTEX_ERR_DST 2
#define MUTEX_ERR_LOCK 3
#define MUTEX_ERR_UNLOCK 4
#define SEM_ERR_CRT 5
#define SEM_ERR_WAIT 6
#define SEM_ERR_POST 7
#define SEM_ERR_DESTROY 8


typedef struct CircularQueue
{ 
	QueueElem *v; // pointer to the queue buffer unsigned int capacity; // queue capacity 
	unsigned int capacity;
	unsigned int first; // head of the queue 
	unsigned int last; // tail of the queue 
	sem_t empty; // semaphores and mutex for implementing the 
	sem_t full; // producer-consumer paradigm pthread_mutex_t mutex; 
	pthread_mutex_t mutex;
} CircularQueue;


void queue_init(CircularQueue **q, unsigned int capacity);

// Inserts 'value' at the tail of queue 'q' 
void queue_put(CircularQueue *q, QueueElem value);
 
// Removes element at the head of queue 'q' and returns its 'value' 
QueueElem queue_get(CircularQueue *q);

// Frees space allocated for the queue elements and auxiliary management data 
void queue_destroy(CircularQueue *q);

void test(int *cenas);


#endif
