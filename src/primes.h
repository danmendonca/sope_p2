#ifndef primes_H_
#define primes_H_

#define QUEUE_END 0
#define THREAD_ERR_DETACH 11
#define THREAD_ERR_CONDWAIT 12
#define THREAD_ERR_DSTR_CW 13

#include <sys/types.h>
#include <sys/mman.h>
#include <math.h>
#include <fcntl.h>
#include <math.h>
#include "circularQueue.h"

typedef struct primeList
{

	pthread_mutex_t mutex;
	pthread_mutex_t thr_cnt_mutex;
	sem_t complete;
	pthread_cond_t cond;

	unsigned int thr_nr;

	int index;
	QueueElem n;
	QueueElem *primes;


} primeList;

void *thrFunc(void *arg);
void *filterThr(void *arg);
void *lastThr(void *arg);

void populate(QueueElem *list, unsigned int last_value);
void sortList(QueueElem *primes, unsigned int size);
void printList(QueueElem *primes, unsigned int size);



#endif
